import React, { Component } from 'react';
import { StyleSheet,Text,View,TouchableOpacity, FlatList, Dimensions,Image,ImageBackground} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class WelcomeAuth extends Component {
    constructor (props){
        super(props);
        this.state = {
            dewi : [
                {gambar:<Image source={require('../assets/2.jpg')}/>,satu : <TouchableOpacity style={{padding:10}} onPress={() => this.props.navigation.navigate('sholat')}><Text style={{color:'white'}}>TATA CARA SHOLAT WAJIB</Text></TouchableOpacity>,no:1},
                {gambar:<Image source={require('../assets/2.jpg')}/>,satu : <TouchableOpacity style={{padding:10}} onPress={() => this.props.navigation.navigate('doa')}><Text style={{color:'white',alignItems:'center',color:'white'}}>DOA DAN DZIKIR SETELAH SHOLAT</Text></TouchableOpacity>,no:2},
            ]
        }
    }
    render (){
    return(
            <View style={{flex:1, alignItems:'center',}}>
            <FlatList
                data={this.state.dewi}
                renderItem = {({item,index}) => (
                    <View style={{alignItems:'center',marginTop:110,backgroundColor:'black'}}>
                        {item.gambar}
                        {item.satu}
                    </View>
                )}
                keyExtractor = {(item) => item.no}
                numColumns = {numColumns}
            />
            </View>
    )
    }
}
export default WelcomeAuth;

const styles = StyleSheet.create({
    sans : {
        height : numColumns / (WIDTH*2),
        padding:5,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'blue',
        fontSize: 50,
        color:'black'

    }

});
