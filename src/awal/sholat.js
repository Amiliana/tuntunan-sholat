import React, { Component } from 'react';
import { StyleSheet,Image,View,Text, FlatList } from 'react-native';

class sholat extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {satu: <Image source={require('../assets/a.jpg')}/>,teks:<Text>1. Niat sholat</Text> ,no:1},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>
                Hal pertama yang dilakukan saat menunaikan salat adalah melafalkan niat. Gerakan saat membaca niat salat, yaitu dengan posisi tubuh yang tegap, menghadap kiblat, dan pandangan mata mengarah ke tempat sujud.</Text> ,no:2},
                {satu: <Image source={require('../assets/b.jpg')}/>,teks:<Text>2. Takbirotul Ihram</Text> ,no:3},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Setelah niat, gerakan salat berikutnya adalah takbiratul ihram. Cara takbiratul ihram, yaitu mengangkat kedua tangan sejajar telinga (untuk laki-laki) dan sejajar dengan dada (bagi perempuan) sambil membaca "Allahu Akbar".
                Saat mengangkat kedua tangan, telapak tangan harus dibuka. Namun, telapak tangan tidak boleh terlalu rapat atau pun terlalu lebar. </Text> ,no:4},
                {satu: <Image source={require('../assets/c.jpg')}/>,teks:<Text>3. Membaca iftitah</Text> ,no:5},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Sesudah takbiratul ihram, kedua tangan lalu disedekapkan di bagian dada. Kemudian, membaca doa iftitah. Doa iftitah ini dilafalkan pada rakaat pertama dalam salat. </Text> ,no:6},
                {satu: <Image source={require('../assets/c.jpg')}/>,teks:<Text>4. Membaca Al-Fatihah</Text> ,no:7},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Apabila telah selesai membaca doa iftitah, dapat dilanjutkan dengan bacaan surah Al-Fatihah. Dengan posisi tangan tetap disedekapkan pada bagian dada.
                Surah Al-Fatihah hukumnya wajib sehingga tidak boleh ditinggalkan. Hal ini telah diterangkan dalam dalil Rasulullah SAW yang menyebutkan bahwa, “Tidak ada shalat bagi orang yang tidak membaca Faatihatul Kitaab” (HR. Al Bukhari 756, Muslim 394)</Text> ,no:8},
                {satu: <Image source={require('../assets/c.jpg')}/>,teks:<Text>5. Membaca surat yang dihafal</Text> ,no:9},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Masih dalam posisi kedua tangan yang disedekapkan pada dada, bacaan salat berikutnya adalah melafalkan surat sekiranya hafal. Bagi anak-anak yang baru belajar, mungkin Mama dan Papa bisa mengajarinya membaca surah-surah pendek saja.</Text> ,no:10},
                {satu: <Image source={require('../assets/d.jpg')}/>,teks:<Text>6. Ruku</Text> ,no:11},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Gerakan salat berikutnya, yaitu ruku. Ruku dilakukan dengan membungkukkan badan hingga membentuk sudut 45°. Dengan bagian punggung dan kepala lurus ke depan, tangan di lutut dengan jari-jari direnggangkan, siku juga sedikit direnggangkan, Ma.
                Saat berpindah gerakan salat menuju ruku, diwajibkan untuk melakukan takbir intiqal atau membaca "Allahu Akbar". Kemudian saat ruku, hendaklah membaca doa ruku.</Text> ,no:12},
                {satu: <Image source={require('../assets/e.jpg')}/>,teks:<Text>7. I'tidal</Text> ,no:13},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Selesainya ruku, gerakan salat berikutnya adalah I'tidal. Dengan kembali berdiri tegak sempurna sambil mengangkat kedua tangan setinggi telinga (bagi laki-laki) dan sejajar dada (untuk perempuan).
                Saat bangkit dari ruku, dianjurkan membaca tasmi' atau bacaan "Sami'allahu liman hamidah".
                Gerakan ini dilakukan sebagai tanda pemisah antara ruku dan sujud.</Text> ,no:14},
                {satu: <Image source={require('../assets/f.jpg')}/>,teks:<Text>8. Sujud 1</Text> ,no:15},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Gerakan salat selanjutnya, yaitu sujud. Dengan membaca takbir  intiqal atau melafalkan "Allahu Akbar" sebelum berpindah gerakan salat. Dilanjutkan dengan membaca doa sujud.
                Sujud dilakukan dengan cara bertumpu pada 7 anggota badan, tangan sejajar pundak atau telinga, tumit rapat, dan ujung kaki menghadap kiblat. </Text> ,no:16},
                {satu: <Image source={require('../assets/g.jpg')}/>,teks:<Text>9. Duduk antara 2 sujud</Text> ,no:17},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Setelah selesai membaca doa sujud, gerakan salat dilanjutkan dengan duduk di antara 2 sujud. Posisinya, yaitu dengan duduk secara tegak, di mana punggung kaki kiri dibentangkan pada lantai dan diduduki, kemudian kaki kanan ditegakkan dan jari-jarinya menghadap kiblat.
                Duduk di antara 2 sujud ini tetap dilakukan dengan membaca takbir intiqal. Lalu, dilanjutkan dengan melafalkan doa antara 2 sujud.</Text> ,no:18},
                {satu: <Image source={require('../assets/h.jpg')}/>,teks:<Text>10. Sujud 2</Text> ,no:19},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Gerakan salat berikutnya dilanjutkan dengan sujud kembali. Dengan melafalkan takbir intiqal dan membaca doa sujud lagi.</Text> ,no:20},
                {satu: <Image source={require('../assets/c.jpg')}/>,teks:<Text>11. Bangkit berdiri rakaat selanjutnya</Text> ,no:21},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Dilanjutkan dengan bangkit berdiri kembali untuk rakaat selanjutnya. Gerakan dan bacaan tetap sama seperti sebelumnya, tetapi jumlah rakaat ini tergantung pada waktu salat. </Text> ,no:22},
                {satu: <Image source={require('../assets/i.jpg')}/>,teks:<Text>12. Duduk tasyahud</Text> ,no:23},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Apabila telah menyelesaikan rakaat salat, maka gerakan selanjutnya adalah duduk tasyahud. Duduk tasyahud dibagi menjadi 2, yaitu tasyahud awal dan tasyahud akhir.
                Pada duduk tasyahud awal, telapak kaki kiri diduduki dan telapak kaki kanan ditegakkan. Sedangkan pada duduk tasyahud akhir, kaki kiri dimajukkan atau dimasukkan ke bawah betis kaki kanan dan telapak kaki kanan ditegakkan.
                Ketika melakukan duduk tasyahud, jari telunjuk tangan kanan diarahkan ke kiblat, telapak tangan kiri diletakkan di atas lutut bagian kiri, dan mata menatap ke arah jari telunjuk.</Text> ,no:24},
                {satu: <Image source={require('../assets/j.jpg')}/>,teks:<Text>13. Salam</Text> ,no:25},
                {satu: <Text style={{textAlign:'justify',marginLeft:15}}>Gerakan terakhir saat menunaikan salat, yaitu salam. Setelah selesai melakukan gerakan salat dan melafalkan bacaan salat keseluruhan, maka ucapkanlah salam.
                Dengan menolehkan kepala ke bagian kanan dan kiri. Setiap menoleh ke kanan juga kiri, disertai dengan bacaan salam yang lengkap, "Assalamualaikum warahmatullah wabarakatuh".</Text> ,no:26},
            ]
        }
    }

    render(){
        return(
            <View style={{alignItems:'center', backgroundColor:'yellow'}}>
                <Text style={{fontSize:20}}>TATA CARA SHOLAT WAJIB SEHARI-HARI</Text>
                <FlatList
                    data ={this.state.dewi}
                    renderItem={({item,index}) => (
                        <View>
                            {item.satu}
                            {item.teks}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                />
            </View>
        )
    }
}
export default sholat;