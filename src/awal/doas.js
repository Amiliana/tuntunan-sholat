import React, { Component } from 'react';
import { StyleSheet,Image,View,Text,FlatList } from 'react-native';

class doas extends Component{
    constructor(props){
        super(props);
        this.state= {
            dewi : [
                {teks:<Text>1. Membaca Istighfar Dahulu</Text> ,no:1},
                {teks:<Text>Sebelum berdoa, dianjurkan untuk membaca istighfar sebanyak tiga kali:</Text> ,no:2},
                {teks:<Text>أَسْتَغْفِرُ اللهَ الْعَظِـيْمِ الَّذِيْ لَااِلَهَ اِلَّا هُوَ الْحَيُّ الْقَيُّوْمُ وَأَتُوْبُ إِلَيْهِ</Text> ,no:3},
                {teks:<Text>"ASTAGHFIRULLAH HAL'ADZIM, ALADZI LAAILAHA ILLAHUWAL KHAYYUL QOYYUUMU WA ATUUBU ILAIIH"</Text> ,no:4},
                {teks:<Text>2. Dilanjutkan dengan membaca :</Text> ,no:5},
                {teks:<Text>لَاإِلَهَ إِلَّا اللهُ وَحْدَهُ لَا شَرِيْكَ لَهُ، لَهُ الْمُلْكُ وَلَهُ الْحَمْدُ يُحْيِيْ وَيُمِيْتُ وَهُوَ عَلَى كُلِّ شَيْئٍ قَدِيْرٌ</Text> ,no:6},
                {teks:<Text>"LAA ILAHA ILLALLAH WAKHDAHU LAA SYARIKA LAHU, LAHUL MULKU WALAHUL KHAMDU YUKHYIIY WAYUMIITU WAHUWA 'ALAA KULLI SYAI'INNQODIIR"</Text> ,no:7},
                {teks:<Text>3. Memohon perlindungan dari siksa neraka, dengan membaca berikut 3 kali:</Text> ,no:8},
                {teks:<Text>اَللَّهُمَّ أَجِرْنِـى مِنَ النَّارِ</Text> ,no:9},
                {teks:<Text>"ALLAHUMMA AJIRNI MINAN-NAAR" 3 x</Text> ,no:10},
                {teks:<Text>4. Memuji Allah Dengan Kalimat</Text> ,no:11},
                {teks:<Text>للَّهُمَّ أَنْتَ السَّلاَمُ، وَمِنْكَ السَّلَامُ، وَإِلَيْكَ يَعُوْدُ السَّلَامُ فَحَيِّنَارَبَّنَا بِالسَّلَامِ وَاَدْخِلْنَا الْـجَنَّةَ دَارَ السَّلَامِ تَبَارَكْتَ رَبَّنَا وَتَعَالَيْتَ يَا ذَاالْـجَلَالِ وَاْلإِكْرَام.</Text> ,no:12},
                {teks:<Text>"ALLAHUMMA ANGTASSALAM, WAMINGKASSALAM, WA ILAYKA YA'UUDUSSALAM FAKHAYYINA RABBANAA BISSALAAM WA-ADKHILNALJANNATA DAROSSALAAM TABAROKTA RABBANAA WATA'ALAYTA YAA DZALJALAALI WAL IKRAAM"</Text> ,no:13},
                {teks:<Text>5. Membaca surat Al-Fatihah dan ayat kursi</Text> ,no:14},
                {teks:<Text>Membaca Surat Al-Fatihah kemudian dilanjutkan dengan membaca Ayat Kursi (Al-Baqarah : 225)</Text> ,no:15},
                {teks:<Text>أَعُوذُ بِاللَّهِ مِنَ الشَّيْطَانِ الرَّجِيمِ. بِسْمِ اللهِ الرَّحْمَنِ الرَّحِيْمِ. اَللهُ لَا إِلَهَ إِلَّا هُوَ الْحَيُّ الْقَيُّومُ لَا تَأْخُذُهُ سِنَةٌ وَّلَانَوْمٌ، لَهُ مَافِي السَّمَاوَاتِ وَمَافِي اْلأَرْضِ مَن ذَا الَّذِيْ يَشْفَعُ عِنْدَهُ إِلَّا بِإِذْنِهِ يَعْلَمُ مَابَيْنَ أَيْدِيْهِمْ وَمَاخَلْفَهُمْ وَلَا يُحِيْطُونَ بِشَيْءٍ مِّنْ عِلْمِهِ إِلَّا بِمَا شَآءَ، وَسِعَ كُرْسِيُّهُ السَّمَاوَاتِ وَاْلأَرْضَ وَلَا يَـؤدُهُ حِفْظُهُمَا وَهُوَ الْعَلِيُّ الْعَظِيْمُ.</Text> ,no:16},
                {teks:<Text>"Allahu laa ilaaha illaa huwal hayyul qayyum. Laa ta'khudzuhuu sinatuw wa laa naum. Lahuu maa fis samaawaati wa maa fil ardh. Man dzal ladzii yasyfa'u 'indahuu illaa bi idznih. Ya'lamu maa bayna aidiihim wa maa khalfahum. Wa laa yuhiithuuna bi syai-im min 'ilmihii illaa bimaa syaa-a. Wasi'a kursiyyuhus samaawaati wal ardh walaa ya-uuduhuu hifzhuhumaa Wahuwal 'aliyyul 'azhiim."</Text> ,no:17},
                {teks:<Text>6. Membaca Tasbih, Tahmid, Takbir, dan Tahlil</Text> ,no:18},
                {teks:<Text>Membaca kalimat Tasbih 33 kali</Text> ,no:19},
                {teks:<Text>سُبْحَانَ اللهِ</Text> ,no:20},
                {teks:<Text>"SUBHANALLAH" 33x</Text> ,no:21},
                {teks:<Text>Membaca kalimat Tahmid 33 kali</Text> ,no:22},
                {teks:<Text>الْحَمْدُلِلهِ</Text> ,no:23},
                {teks:<Text>"ALHAMDULILLAH" 33x</Text> ,no:24},
                {teks:<Text>Membaca kalimat Takbir 33 kali</Text> ,no:25},
                {teks:<Text>اللهُ اَكْبَرُ</Text> ,no:26},
                {teks:<Text>"ALLAHU AKBAR"</Text> ,no:27},
                {teks:<Text>Membaca kalimat Tahlil 33 kali</Text> ,no:28},
                {teks:<Text>لَااِلٰهَ اِلَّا اللهُ</Text> ,no:29},
                {teks:<Text>"LAILAHA ILLALLAH"</Text> ,no:30},
                {teks:<Text>7. Membaca Doa Berikut</Text> ,no:31},
                {teks:<Text>Setelah selesai berdzikir, maka membaca doa setelah sholat berikut</Text> ,no:32},
                {teks:<Text>بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم</Text> ,no:33},
                {teks:<Text>اَلْحَمْدُ لِلَّهِ رَبِّ الْعَالَمِيْنَ. حَمْدًا يُوَافِيْ نِعَمَهُ وَيُكَافِئُ مَزِيْدَهُ. يَا رَبَّنَا لَكَ الْحَمْدُ كَمَا يَنْبَغِيْ لِجَلاَلِ وَجْهِكَ الْكَرِيْمِ وَعَظِيْمِ سُلْطَانِكَ</Text> ,no:34},
                {teks:<Text>"BISMILLAHIRRAHMAANIRRAHIIM. ALHAMDU LILLAAHI RABBIL 'AALAMIIN, HAMDAN YUWAAFII NI'AMAHU WAYUKAAFII MAZIIDAHU. YA RABBANAA LAKAL HAMDU KAMAA YAN BAGHHI LIJALAALI WAJHIKA WA'AZHIIMI SULTHAANIKA."</Text> ,no:35},
                {teks:<Text>اللهم صل على سيدنا محمد وعلى ال سيدنا محمد</Text> ,no:36},
                {teks:<Text>"ALLAHUMMA SHALLI 'ALAA SAYYIDINAA MUHAMMADIN WA'ALAA AALI SAYYIDINAA MUHAMMAD".</Text> ,no:37},
                {teks:<Text>اَللَّهُمَّ رَبَّنَا تَـقَـبَّلْ مِنَّا صَلاَتَنَا وَصِيَا مَنَا وَرُكُوْ عَنَا وَسُجُوْدَنَا وَقُعُوْدَنَا وَتَضَرُّ عَنَا وَتَخَشُّوْ عَنَا وَتَعَبُّدَنَا وَتَمِّمْ تَقْصِيْرَ نَا يَا اَلله يَا رَبَّ الْعَا لَمِيْنَ.</Text> ,no:38},
                {teks:<Text>"ALLAHUMMA RABBANAA TAQABBAL MINNAA SHALAATAANA WASHIYAAMANAA WARUKUU'ANAA WASUJUUDANAA WAQU'UUDANAA WATADLARRU'ANAA, WATAKHASYSYU'ANAA WATA'ABBUDANAA, WATAMMIM TAQSHIIRANAA YAA ALLAH YAA RABBAL'AALAMIIN".</Text> ,no:39},
                {teks:<Text>رَبَّنَا ضَلَمْنَا أَنْفُسَنَا وَإِنْ لَمْ تَغْفِرْ لَنَا وَتَرْ حَمْنَا لَنَكُوْ نَنَّ مِنَ الْخَا سِرِ يْنَ</Text> ,no:40},
                {teks:<Text>"RABBANA DZHALAMNAA ANFUSANAA WA-INLAMTAGHFIR LANA WATARHAMNAA LANAKUUNANNA MlNAL KHAASIRIIN".</Text> ,no:41},
                {teks:<Text>رَبَّنَا وَلاَ تَحْمِلْ عَلَيْنَا إِصْرًا كَمَا حَمَلْتَهُ عَلَى الَّذِ يْنَ مِنْ قَبْلِنَا</Text> ,no:42},
                {teks:<Text>"RABBANAA WALAA TAHMIL'ALAINAA ISHRAN KAMA HAMALTAHUL'ALAL LADZIINA MIN QABLINAA."</Text> ,no:43},
                {teks:<Text>رَبَّنَا وَلاَ تُحَمِّلْنَا مَا لاَ طَا قَتَا لَنَا بِهِ, وَاعْفُ عَنَّا وَاغْفِرْلَنَا وَارْحَمْنَا أَنْتَ مَوْلاَ نَا فَا نْصُرْنَا عَلَى الْقَوْمِ الْكَا فِرِيْنَ</Text> ,no:44},
                {teks:<Text>"RABBANAA WALAA TUHAMMILNAA MAALAA THAAQATA LANAA BIHII WA'FU'ANNAA WAGHFIR LANAA WARHAMNAA ANTA MAULAANAA FANSHURNAA 'ALAL QAUMIL KAAFIRIIN".</Text> ,no:45},
                {teks:<Text>رَبَّنَا لاَ تُزِغْ قُلُوْ بَنَا بَعْدَ إِذْ هَدَ يْتَنَا وَهَبْ لَنَا مِنْ لَّدُ نْكَ رَحْمَةً إِنَّكَ أَنْتَ الْوَهَّابُ</Text> ,no:46},
                {teks:<Text>"RABBANAA LAA TUZIGH QULUUBANAA BA'DA IDZHADAITANAA W'AHABLANAA MIN LADUNKA RAHMATAN INNAKA ANTAL WAHHAAB".</Text> ,no:47},
                {teks:<Text>رَبَّنَا غْفِرْلَنَا وَلِوَالِدِيْنَ وَلِجَمِيْعِ الْمُسْلِمِيْنَ وَالْمُسْلِمَاتِ وَالْمُؤْمِنِيْنَ وَالْمُؤْمِنَاتِ أَلْأَ حْيَآءِمِنْهُمْ وَاْلأَ مْوَاتِ, اِنَّكَ عَلَى قُلِّ ثَيْءٍقَدِيْرِ</Text> ,no:48},
                {teks:<Text>"RABBANAGHFIR LANAA WALIWAALIDINAA WALIJAMI'IL MUSLIMIIN WALMUSLIMAATI WAL MU'MINIINA WALMU'MINATI. AL AHYAA-I-MINHUM WAL AMWAATI, INNAKA ALAA KULI SYAI'N QADIIR".</Text> ,no:49},
                {teks:<Text>رَبَّنَا آتِنَا فِي الدُّنْيَا حَسَنَةً وَفِي اْلآ خِرَةِ حَسَنَةً وَقِنَا عَذَابَ النَّارِ</Text> ,no:50},
                {teks:<Text>"RABBANAA AATINAA FIDDUNYAA HASANATAN WAFIL AAKHIRATI HASANATAN WAQINAA ADZAABAN-NAAR".</Text> ,no:51},
                {teks:<Text>اللهم اغفر لنا ذنوبناوكفرعنا سيئاتنا وتوفنا مَعَ الْأَ بْرَارِ</Text> ,no:52},
                {teks:<Text>"ALLAHUMMAGHFIRLANAA DZUNUUBANAA WAKAFFIR ANNAA SAYYIAATINAA WATAWAFFANAA MAALABRAARI".</Text> ,no:53},
                {teks:<Text>سُبْحَانَ رَبِّكِ رَبِّ الْعِزَةِ عَمَّا يَصِفُوْنَ، وَسَلاَمٌ عَلَى الْمُرْ سَلِيْنَ، وَالْحَمْدُ لِلهِ رَبِّ الْعَالَمِيْنَ</Text> ,no:54},
                {teks:<Text>"SUBHAANA RABBIKA RABBIL I'ZZATI AMMAA YASHIFUUNA WASALAAMUN 'ALAL MURSALHNA WAL-HAMDU LILLAAHI RABBIL'AALAMIINA".</Text> ,no:55},
                {teks:<Text>Artinya: ""Dengan menyebut nama Allah Yang Maha Pengasih dan Maha Penyayang.</Text> ,no:56},
                {teks:<Text>Segala puji bagi Allah Tuhan seru sekalian alam. Dengan puji yang sebanding dengan nikmat-Nya dan menjamin tambahannya. Ya Allah Tuhan Kami, bagi-Mu segala puji dan segala apa yang patut atas keluhuran DzatMu dan Keagungan kekuasaanMu. "Ya Allah! Limpahkanlah rahmat dan salam atas junjungan kita Nabi Muhammad dan sanak keluarganya.</Text> ,no:57},
                {teks:<Text>Ya Allah terima sholat kami, puasa kami, ruku kami, sujud kami, duduk rebah kami, khusyu' kami, pengabdian kami, dan sempurnakanlah apa yang kami lakukan selama sholat ya Allah. Tuhan seru sekalian alam.</Text> ,no:58},
                {teks:<Text>Ya Allah, Kami telah aniaya terhadap diri kami sendiri, karena itu ya Allah jika tidak dengan limpahan ampunan-Mu dan rahmat-Mu niscaya kami akan jadi orang yang sesat. Ya Allah Tuhan kami, janganlah Engkau pikulkan atas diri kami beban yang berat sebagaimana yang pernah Engkau bebankan kepada orang yang terdahulu dari kami. Ya Allah Tuhan kami, janganlah Engkau bebankan atas diri kami apa yang di luar kesanggupan kami. Ampunilah dan limpahkanlah rahmat ampunan terhadap diri kami ya Allah. Ya Allah Tuhan kami, berilah kami pertolongan untuk melawan orang yang tidak suka kepada agamaMu.</Text> ,no:59},
                {teks:<Text>Ya Allah Tuhan kami, janganlah engkau sesatkan hati kami sesudah mendapat petunjuk, berilah kami karunia. Engkaulah yang maha Pemurah.</Text> ,no:60},
                
                {teks:<Text>Maha suci Engkau, Tuhan segala kemuliaan. Suci dari segala apa yang dikatakan oleh orang-orang kafir. Semoga kesejahteraan atas para Rasul dan segala puji bagi Allah Tuhan seru sekalian alam."</Text> ,no:62},
            ]
        }
    }

    render() {
        return (
            <View>
                <FlatList
                data={this.state.dewi}
                renderItem = {({item,index}) => (
                    <View>
                        {item.teks}
                    </View>
                )}
                keyExtractor = {(item) => item.no}
            />
            </View>
        )
    }
}

export default doas;