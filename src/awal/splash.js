import React, {useEffect} from 'react';
import {View,Text,StyleSheet,Image } from 'react-native';


const splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('welcome');
        }, 5000);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={require('../assets/3.jpg')}/>
            <Text style={styles.welcomeText}>Tuntunan Shalat Dan Doa Lengkap</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper :{
        flex:1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    welcomeText: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'maroon',
        paddingBottom: 30,
    },
});
export default splash;