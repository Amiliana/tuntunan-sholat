import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Splash from '../awal/splash';
import Welcome from '../awal/welcome';
import Sholat from '../awal/sholat';
import Doa from '../awal/doa';
import Doas from '../awal/doas';
import Dzikir from '../awal/dzikir';

const Stack = createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen
                name = "splash"
                component = {Splash}
                options = {{headerShown: false}}
            />
            <Stack.Screen
                name = "welcome"
                component = {Welcome}
                options = {{headerShown: false}}
            />
            <Stack.Screen
                name = "sholat"
                component = {Sholat}
            />
            <Stack.Screen
                name = "doa"
                component = {Doa}
            />
            <Stack.Screen
                name = "doas"
                component = {Doas}
            />
            <Stack.Screen
                name = "dzikir"
                component = {Dzikir}
            />
           
        </Stack.Navigator>
    )
}

export default Route;